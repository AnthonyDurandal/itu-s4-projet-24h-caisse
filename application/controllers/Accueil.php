<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accueil extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('DBTable');
		$req = "select * from categorie";
		$data['listeCategorie'] = $this->DBTable->find($req); 
		$data['page'] = ""; 
		$this->load->view('template.php',$data);
	}	
	public function	listeProduit($categorie)
	{
		$this->load->model('DBTable');
		$req = "select * from categorie";
		$req2 = "select produit.id as idProduit,
		categorie.id as idCategorie,
		produit.intitule as nomProduit,
		categorie.intitule as nomCategorie,
		prixProduit,
		img 
		from produit 
		join categorie 
		on produit.idCategorie = categorie.id
		join majPrixProduit
		on produit.id = majPrixProduit.idProduit
		where categorie.intitule='".$categorie."'";

		$data['listeCategorie'] = $this->DBTable->find($req);
		$data['listeProduit'] = $this->DBTable->find($req2);
		$data['page'] = "listeProduit.php";  
		$this->load->view('template.php',$data);
	}	
}

?>