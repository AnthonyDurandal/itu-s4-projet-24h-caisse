<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CRUDProduit extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function rechercheProduit($nom, $categorie, $prixMin, $prixMax)
	{
		$req = "select produit.id as idProduit,
		categorie.id as idCategorie,
		produit.intitule as nomProduit,
		categorie.intitule as nomCategorie,
		prixProduit,
		img 
		from produit 
		join categorie 
		on produit.idCategorie = categorie.id
		join majPrixProduit
		on produit.id = majPrixProduit.idProduit";
		$condition =" where ";
		if($nom != "")$condition += "nomProduit='".$nom."' and ";
		if($categorie != "")$condition += "nomCategorie='".$categorie."' and ";
		if($prixMin != "")$condition += "prixProduit >='".$prixMin."' and ";
		if($prixMax != "")$condition += "prixProduit<='".$prixMax."' and ";

		if(strlen($condition)==7)$condition = "";
		$req +=  $condition;
		$req = substr($req, 0, -5);
		$query = $this->db->query($req);
		$tableau = [];
		foreach ($query->result_array() as $row) {
			$tableau[] = $row;
		}
		return $tableau;
	}	
	public function insererProduit($nomProduit, $nomCategorie, $prix,$img)
	{
		$reqIdCateg = "select id from categorie where intitule='".$nomCategorie."'";
		$query = $this->db->query($reqIdCateg);
		$nomCategorie = $query->result_array()[0];
		$req = "insert into produit values(null,".$nomCategorie['id'].",'".$nomProduit."','".$img."')";
		$query = $this->db->query($req);
		$reqIdProduit = "select id from produit order by id";
		$query = $this->db->query($reqIdProduit);
		$idProduit = $query->result_array()[0];
		$reqPrix = "insert into majPrixProduit values(null,".$idProduit['id'].",'SYSDATE()',".$prix.")";
		$query = $this->db->query($reqIdProduit);
	}

	public function updateProduit($idProduit, $idCategorie, $intitule, $prix)
	{
		$set = "";
		if($idCategorie!="")$set += "idCategorie='".$idCategorie."',"
		if($intitule!="")$set += "intitule='".$intitule."',"
		$req = "update produit set ".$set;
		$req = substr($req,0,strlen($res)-1);
		$query = $this->db->query($req);

		if($prix!="")
		{
			$req2 = "update majPrixProduit set prixProduit='".$prix."' where idProduit='".$idProduit."'"
			$query = $this->db->query($req2);
		}
	}

?>