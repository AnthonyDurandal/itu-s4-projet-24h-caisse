<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('sep_millier')) {
	function sep_millier($nbr) {
		return number_format($nbr,2,"."," ");
	}
}

if ( ! function_exists('format_monnaie')) {
	function format_monnaie($prix) {
		return sep_millier($prix)." Ar";
	}
}

?>