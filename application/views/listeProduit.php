<div style="position: relative;padding-top: 10vh;">
<div class="row row-cols-1 row-cols-xs-2 row-cols-sm-2 row-cols-lg-4 g-3">
    <?php for ($i=0; $i < count($listeProduit); $i++) { ?>
    <div class="col">
        <div class="card h-100 shadow-sm"> <img src="<?php echo img_url($listeProduit[$i]['img']) ?>" class="card-img-top" alt="...">
            <div class="card-body">
                <div class="clearfix mb-3"> <span class="float-start badge rounded-pill bg-primary">
                    <?php echo $listeProduit[$i]['nomProduit'] ?></span></div>
                <div> <span class="float-end price-hp"><?php echo format_monnaie($listeProduit[$i]['prixProduit']) ?></span> </div>
                <div class="text-center my-4"> <a href="#" class="btn btn-warning">+</a> </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<div style="margin-top: 20px;">
      <div style="float:left">
        <a href="#" class="previous" >&laquo; Previous</a>
      </div>
      <div style="text-align: right">
        <a href="#" class="next" >Next &raquo;</a>
      </div>
    </div>