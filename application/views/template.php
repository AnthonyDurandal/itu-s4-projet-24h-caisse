<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.87.0">
    <title>Caisse Enregistreuse</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/cheatsheet/">

    

    <!-- Bootstrap core CSS -->
    <link href="<?php echo css_url('bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- css -->
    <link href="<?php echo css_url('style.css'); ?>" rel="stylesheet">
    <link href="<?php echo css_url('card.css'); ?>" rel="stylesheet">
    <link href="<?php echo css_url('w3.css'); ?>" rel="stylesheet">
  </head>
<body class="bg-light">
    
<header class="bd-header py-3 d-flex align-items-stretch border-bottom " style="background: rgb(28,61,78);">
  <div class="container-fluid d-flex align-items-center">
    <h1 class="d-flex align-items-center fs-4 text-white mb-0">
      <strong>Caisse</strong>Enregistreuse
    </h1>
  </div>
</header>
<div class="content col-md-12">
  <div class="categorie col-md-2">
    <div class="detailCategorie col-md-12">
    <h1>Catégories </h1>
    <hr>
      <?php
        for ($i=0; $i < count($listeCategorie); $i++) {
      ?>
      <a class="lienCateg" href="<?php echo site_url("Accueil/listeProduit/".$listeCategorie[$i]['intitule']) ?>">
      <div class="w3-card-4">
        <div class="w3-container w3-center" style="padding-top: 10px;">
          <p><?php echo $listeCategorie[$i]['intitule'] ?></p>
        </div>
      </div>
      </a>
      <br>
      <?php } ?>
    </div>
  </div>
  <div class="ticket col-md-3">
    <div class="detailTicket col-md-12">
      <h1 style="text-align: right;color:white;">00.00 Ar</h1>
    </div>
    <div class="listeAchatNonValide col-md-12">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Produit</th>
            <th scope="col">Nombre</th>
            <th scope="col">Prix Unitaire</th>
          </tr>
        </thead>
        <tbody>
          <?php for ($i=0; $i < 0; $i++) { ?>
          <tr>
            <td>Produit<?php echo $i ?></td>
            <td>6</td>
            <td>1000 Ar</td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="validation col-md-12">
      <div class="btnValider col-md-12">
        <button type="button" class="btn btn-labeled btn-success">
                  <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>VALIDER
        </button>
      </div>
    </div>
  </div>
  <div class="listeProduit offset-2 col-md-7">
    <?php if($page!="")include($page); ?>
  </div>
  </div>
</div>

    <script src="<?php echo js_url('bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?php echo js_url('cheatsheet.js'); ?>"></script>
</body>
</html>
