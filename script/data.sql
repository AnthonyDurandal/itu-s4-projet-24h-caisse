
-- caisse
insert into caisse values(null);
insert into caisse values(null);
insert into caisse values(null);
insert into caisse values(null);
insert into caisse values(null);

insert into categorie values(null,'PPN');
insert into categorie values(null,'Sanitaire');
insert into categorie values(null,'Boisson');
insert into categorie values(null,'Jouets');

insert into produit values(null,1,'riz 5kg','riz.jpg');
insert into produit values(null,1,'Eau naturelle','eau.jpg');
insert into produit values(null,1,'Gant','gant2.jpg');
insert into produit values(null,1,'fil dentaire','fil.jpg');

insert into produit values(null,2,'Savon de marseille en paquet','savon.jpg');
insert into produit values(null,2,'dentifrice Oral Expert','oralb.jpg');
insert into produit values(null,2,'dentifrice Colgate','colgate.jpg');
insert into produit values(null,2,'Gant de toilette','gant.jpg');

insert into produit values(null,3,'Coca cola 1L','coca.jpg');
insert into produit values(null,3,'Sprite 1.5L','sprite.jpg');
insert into produit values(null,3,'Fanta 50cl','fanta.jpg');
insert into produit values(null,3,'Orangina 50cl','orangina.jpg');

insert into produit values(null,4,'Armure iron man pm','iron.jpg');
insert into produit values(null,4,'Ballon basket taille 7','ballon.jpg');
insert into produit values(null,4,'Voiture t\él\écommand\ée','voiture.jpg');
insert into produit values(null,4,'Drone');



-- vagueProduit

insert into vagueProduit values(null,1,20,'2021-01-01','2021-02-01','2021-07-01');
insert into vagueProduit values(null,1,10,'2021-03-01','2021-04-01','2021-10-01');
insert into vagueProduit values(null,1,20,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,2,15,'2021-02-01','2021-02-22','2021-09-01');
insert into vagueProduit values(null,2,10,'2021-03-01','2021-04-01','2021-10-01');
insert into vagueProduit values(null,2,10,'2021-07-01','2021-09-01','2021-12-01');

insert into vagueProduit values(null,3,20,'2021-01-01','2021-02-01','2021-07-01');
insert into vagueProduit values(null,3,20,'2021-03-01','2021-04-01','2021-10-01');
insert into vagueProduit values(null,3,20,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,4,50,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,5,10,'2021-05-01','2021-06-01','2021-12-01');
insert into vagueProduit values(null,5,10,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,6,10,'2021-05-01','2021-06-01','2021-12-01');
insert into vagueProduit values(null,6,20,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,7,20,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,8,40,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,8,30,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,9,25,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,10,25,'2021-05-01','2021-06-01','2021-12-01');
insert into vagueProduit values(null,10,10,'2021-06-01','2021-07-01','2022-12-01');
insert into vagueProduit values(null,10,30,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,11,10,'2021-04-01','2021-07-01','2021-12-01');
insert into vagueProduit values(null,11,10,'2021-07-01','2021-08-01','2021-12-01');

insert into vagueProduit values(null,12,50,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,13,25,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,14,20,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,15,50,'2021-05-01','2021-06-01','2021-12-01');

insert into vagueProduit values(null,16,50,'2021-05-01','2021-06-01','2021-12-01');




-- achat
insert into achat values(null,1,'2021-08-01');
insert into achat values(null,2,'2021-07-01');
insert into achat values(null,4,'2021-08-01');
insert into achat values(null,1,'2021-07-01');
insert into achat values(null,2,'2021-08-06');

insert into achat values(null,3,'2021-05-01');
insert into achat values(null,4,'2021-08-01');
insert into achat values(null,1,'2021-07-01');
insert into achat values(null,4,'2021-08-01');
insert into achat values(null,2,'2021-07-01');


-- detailsAchat
insert into detailsAchat values(null,1,1,2);
insert into detailsAchat values(null,1,4,4);
insert into detailsAchat values(null,1,6,1);

insert into detailsAchat values(null,1,2,3);
insert into detailsAchat values(null,1,2,5);


-- miseAJourProduit

insert into majPrixProduit values(null,1,'2021-01-01',1000);
insert into majPrixProduit values(null,2,'2021-01-01',2000);
insert into majPrixProduit values(null,3,'2021-01-01',3000);
insert into majPrixProduit values(null,4,'2021-01-01',4000);
insert into majPrixProduit values(null,5,'2021-01-01',10000);
insert into majPrixProduit values(null,6,'2021-01-01',1500);
insert into majPrixProduit values(null,7,'2021-01-01',3000);
insert into majPrixProduit values(null,8,'2021-01-01',4000);
insert into majPrixProduit values(null,9,'2021-01-01',500);
insert into majPrixProduit values(null,10,'2021-01-01',500);
insert into majPrixProduit values(null,11,'2021-01-01',750);
insert into majPrixProduit values(null,12,'2021-01-01',1000);
insert into majPrixProduit values(null,13,'2021-01-01',40000);
insert into majPrixProduit values(null,14,'2021-01-01',50000);
insert into majPrixProduit values(null,15,'2021-01-01',17090);
insert into majPrixProduit values(null,16,'2021-01-01',100000);

-- remise

insert into remise values(null,1,10,'2021-08-20','2021-09-20');
insert into remise values(null,1,15,'2021-08-25','2021-09-20');
insert into remise values(null,1,20,'2021-01-20','2021-02-20');


-- validationAchat

insert into validationAchat values(null,1,'2021-08-24');
insert into validationAchat values(null,2,'2021-07-01');
insert into validationAchat values(null,3,'2021-08-24');
insert into validationAchat values(null,5,'2021-07-01');
insert into validationAchat values(null,7,'2021-08-01');
insert into validationAchat values(null,10,'2021-07-24');



insert into admin values(null,'admin',sha1('admin'));


select produit.id as idProduit,
categorie.id as idCategorie,
produit.intitule as nomProduit,
categorie.intitule as nomCategorie,
prixProduit,
img 
from produit 
join categorie 
on produit.idCategorie = categorie.id
join majPrixProduit
on produit.id = majPrixProduit.idProduit
where categorie.intitule='PPN'