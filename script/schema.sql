-- mysql -u root -p
-- root
drop database caisse;
create database caisse;
use caisse;

create table caisse(
	id int primary key not null auto_increment
)ENGINE=InnoDB;

create table achat(
	id int primary key not null auto_increment,
	idCaisse int not null,
	dateAchat date not null,
	foreign key(idCaisse) references caisse(id)
)ENGINE=InnoDB;

create table categorie(
	id int primary key not null auto_increment,
	intitule varchar(50) not null
);

create table produit(
	id int primary key not null auto_increment,
	idCategorie int not null,
	intitule varchar(50) not null,
	img varchar(100),
	foreign key(idCategorie) references categorie(id)
)ENGINE=InnoDB;

create table vagueProduit(
	id int primary key not null auto_increment,
	idProduit int not null,
	nombre int not null,
	dateCreation date not null,
	dateArrivee date not null,
	datePeremtion date not null,
	foreign key(idProduit) references produit(id)
)ENGINE=InnoDB;

create table detailsAchat(
	id int primary key not null auto_increment,
	idAchat int not null,
	idVagueProduit int not null,
	quantite int not null,
	foreign key(idAchat) references achat(id),
	foreign key(idVagueProduit) references vagueProduit(id)
)ENGINE=InnoDB;

create table majPrixProduit(
	id int primary key not null auto_increment,
	idProduit int not null,
	dateMaj date not null,
	prixProduit float not null,
	foreign key(idProduit) references produit(id)
)ENGINE=InnoDB;

create table remise(
	id int primary key not null auto_increment,
	idProduit int not null,
	pourcentage float not null,
	dateDebutRemise date not null,
	dateFinRemise date not null,
	foreign key(idProduit) references produit(id)
)ENGINE=InnoDB;

create table validationAchat(
	id int primary key not null auto_increment,
	idAchat int not null,
	dateValidation date not null,
	foreign key(idAchat) references achat(id)
)ENGINE=InnoDB;

create table admin(
	id int primary key not null auto_increment,
	email varchar(100) not null unique,
	mdp varchar(255)not null
)ENGINE=InnoDB;
